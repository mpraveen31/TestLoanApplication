package com.business;

import java.io.File;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginModule {
	
	ChromeDriver cdriver=null;
	String relativePathDriver="driver/chromedriver.exe";
	String absolutePathDriver="";
	File fl=null;
	
	@BeforeTest
	public void beforeTest(){
		absolutePathDriver=new File(relativePathDriver).getAbsolutePath();
		System.setProperty("webdriver.chrome.driver", absolutePathDriver);
		cdriver=new ChromeDriver();
		cdriver.manage().window().maximize();
	}
		
	
  @Test
  public void launchChrome() {
	  System.out.println("Relative Path is absolutePathDriver " + absolutePathDriver);
	  cdriver.get("http://10.117.189.76:8080/INGORION");
  
  }
}
