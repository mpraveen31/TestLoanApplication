package com.loan.application;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static org.testng.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;




public class RestLoginTest {
  URL url=null;
  String resourceUrl="http://10.117.189.76:8080/INGORION";
  String resourceId="";
  
  int expectedStatusCode=200;
  int actualStatusCode;
  Response httpResponse=null;
	
	@Test
  public void validateStatusCodeGet() throws MalformedURLException{	
		RestAssured.baseURI=resourceUrl;
		RequestSpecification requestSpecification=RestAssured.given();
		httpResponse=requestSpecification.request(Method.GET,resourceUrl);
		actualStatusCode=httpResponse.getStatusCode();	
		System.out.println("actualStatusCode " + actualStatusCode);
		
		Assert.assertEquals(actualStatusCode, expectedStatusCode);
	  
  }
	
	
	@Test(enabled=false)
	  public void validateApplicationType() throws MalformedURLException{	
			RestAssured.baseURI=resourceUrl;
			RequestSpecification requestSpecification=RestAssured.given();
			httpResponse=requestSpecification.request(Method.GET,resourceUrl);
			System.out.println("Content Type " + httpResponse.getContentType());	  
	  }
	
	@Test(enabled=false)
	  public void validateStatusLine() throws MalformedURLException{	
			RestAssured.baseURI=resourceUrl;
			RequestSpecification requestSpecification=RestAssured.given();
			httpResponse=requestSpecification.request(Method.GET,resourceUrl);
			System.out.println("Response Body " + httpResponse.getStatusLine());	  
	  }
	
	
	
	@Test(enabled=false)
	  public void validateStatusCodePost() throws MalformedURLException{	
			RestAssured.baseURI=resourceUrl;
			RequestSpecification requestSpecification=RestAssured.given();
			httpResponse=requestSpecification.request(Method.GET,resourceUrl);
			actualStatusCode=httpResponse.getStatusCode();	
			//System.out.println("actualStatusCode " + actualStatusCode);
			Assert.assertEquals(actualStatusCode, 200);
		  
	  }
	
}
